baremodule TypeCompletion

export complete_overload, complete

"""
    complete_overload(t::Type)::Type{<:t}

Implement for your type `t`. The returned value should subtype `t`.

Don't call this function directly. That's what [`complete`](@ref) is for.
"""
function complete_overload(::Type{T}) where {T}
  T
end

"""
    complete(t::Type)::Type{<:t}

Returns a subtype of `t` that may be more specific than `t`.

Don't add methods to this function. That's what [`complete_overload`](@ref) is for.
"""
function complete(::Type{T}) where {T}
  complete_overload(T)::Type::Type{<:T}
end

end
