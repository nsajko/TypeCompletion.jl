using TypeCompletion
using Test
using Aqua: Aqua

struct E0 end
struct E1{Length,R<:NTuple{Length,Nothing}} end
struct E2{A, B<:AbstractVector{A}} end

const nonredundant_alias_E1 = E1{L,NTuple{L,Nothing}} where {L}
TypeCompletion.complete_overload(::Type{E1{L}}) where {L} = nonredundant_alias_E1{L}
TypeCompletion.complete_overload(::Type{E1{<:Any,NTuple{L,Nothing}}}) where {L} = nonredundant_alias_E1{L}

function TypeCompletion.complete_overload(::Type{E2{<:Any,B}}) where {A,B<:AbstractVector{A}}
  E2{A,B}
end

@testset "TypeCompletion.jl" begin
  @testset "Code quality (Aqua.jl)" begin
    Aqua.test_all(TypeCompletion)
  end

  @testset "fallback" begin
    @test complete(E0) == E0
  end

  @testset "nontype" begin
    @test_throws MethodError complete(7)
  end

  @testset "example usage" begin
    @test complete(E1{3}) == complete(E1{<:Any,NTuple{3,Nothing}}) == complete(E1{3,NTuple{3,Nothing}}) == E1{3,NTuple{3,Nothing}}
    @test complete(E2{<:Any,Vector{Int}}) == E2{Int,Vector{Int}}
  end
end
